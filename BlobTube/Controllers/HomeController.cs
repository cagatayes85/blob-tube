﻿using BlobTube.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.RetryPolicies;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace BlobTube.Controllers
{
    public class HomeController : Controller
    {

        static AzureCredentialClass instance = new AzureCredentialClass();
        public ActionResult About()
        {

            return View();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetBlobsAsJson()
        {
            List<CloudFile> cloudFiles = new List<CloudFile>();
            CloudFile blobFile;
            CloudFilesModel blobsList = new
              CloudFilesModel(instance.StoreContainer.ListBlobs(useFlatBlobListing: true));
            CloudBlockBlob blob;
            for (int i = 0; i < blobsList.Files.Count; i++)
            {
                blobFile = blobsList.Files[i];
                blob = instance.StoreContainer.GetBlockBlobReference(blobFile.FileName);
                var sas = blob.GetSharedAccessSignature(new SharedAccessBlobPolicy()
                {
                    Permissions = SharedAccessBlobPermissions.Read,
                    SharedAccessExpiryTime = DateTime.UtcNow.AddHours(1000),//Set this date/time according to your requirements
                });
                blobFile.URL = blob.Uri.ToString();
                blobFile.URI = string.Format("{0}{1}", blob.Uri, sas);//This is the URI which should be embedded in your video player
                cloudFiles.Add(blobFile);
            }
            return Json(cloudFiles, JsonRequestBehavior.AllowGet);
        }




        //SetMetadata
        [HttpPost]
        public ActionResult SendFileMeta(int blocksCount, string fileName, long fileSize)
        {
            instance.StoreContainer.CreateIfNotExists();
            var fileToUpload = new CloudFile()
            {
                BlockCount = blocksCount,
                FileName = fileName,
                Size = fileSize,
                BlockBlob = instance.StoreContainer.GetBlockBlobReference(fileName),
                StartTime = DateTime.Now,
                IsUploadCompleted = false,
                UploadStatusMessage = string.Empty,
            };

            Session.Add("CurrentFile", fileToUpload);
            return Json(true);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UploadPiece(int id)
        {
            HttpPostedFileBase request = Request.Files["Slice"];
            byte[] chunk = new byte[request.ContentLength];

            request.InputStream.Read(chunk, 0, Convert.ToInt32(request.ContentLength));
            JsonResult returnData = null;
            string fileSession = "CurrentFile";
            if (Session[fileSession] != null)
            {
                CloudFile model = (CloudFile)Session[fileSession];

                returnData = UploadLatestPiece(model, chunk, id);
                if (returnData != null)
                {
                    return returnData;
                }
                if (id == model.BlockCount)
                {
                    return FinalChunkCommitment(model);
                }
            }
            else
            {
                returnData = Json(new
                {
                    error = true,
                    isLastBlock = false,
                    message = string.Format(CultureInfo.CurrentCulture,
                        "Failed to Upload file.", "Session Timed out")
                });
                return returnData;
            }

            return Json(new { error = false, isLastBlock = false, message = string.Empty });
        }

        private ActionResult FinalChunkCommitment(CloudFile model)
        {
            model.IsUploadCompleted = true;
            bool errorInOperation = false;
            try
            {
                var blockList = Enumerable.Range(1, (int)model.BlockCount).ToList<int>().ConvertAll(rangeElement =>
                            Convert.ToBase64String(Encoding.UTF8.GetBytes(
                                string.Format(CultureInfo.InvariantCulture, "{0:D4}", rangeElement))));
                model.BlockBlob.PutBlockList(blockList);
                var duration = DateTime.Now - model.StartTime;
                float fileSizeInKb = model.Size / 1024;
                string fileSizeMessage = fileSizeInKb > 1024 ?
                    string.Concat((fileSizeInKb / 1024).ToString(CultureInfo.CurrentCulture), " MB") :
                    string.Concat(fileSizeInKb.ToString(CultureInfo.CurrentCulture), " KB");
                model.UploadStatusMessage = string.Format(CultureInfo.CurrentCulture,
                    "SUccess in file. {0} took {1} seconds uploading time.",
                    fileSizeMessage, duration.TotalSeconds);
            }
            catch (StorageException e)
            {
                model.UploadStatusMessage = "Failed to Upload file. Exception - " + e.Message;
                errorInOperation = true;
            }
            finally
            {
                Session.Remove("CurrentFile");
            }
            return Json(new
            {
                error = errorInOperation,
                isLastBlock = model.IsUploadCompleted,
                message = model.UploadStatusMessage
            });
        }

        private JsonResult UploadLatestPiece(CloudFile _model, byte[] _chunk, int _id)
        {
            using (var chunkStream = new MemoryStream(_chunk))
            {
                var blockId = Convert.ToBase64String(Encoding.UTF8.GetBytes(
                        string.Format(CultureInfo.InvariantCulture, "{0:D4}", _id)));
                try
                {
                    _model.BlockBlob.PutBlock(
                        blockId,
                        chunkStream, null, null,
                        new BlobRequestOptions()
                        {
                            RetryPolicy = new LinearRetry(TimeSpan.FromSeconds(12), 4)
                        },
                        null);
                    return null;
                }
                catch (StorageException exp)
                {
                    Session.Remove("CurrentFile");
                    _model.IsUploadCompleted = true;
                    _model.UploadStatusMessage = "Failed to Upload file. Exception - " + exp.Message;
                    return Json(new { error = true, isLastBlock = false, message = _model.UploadStatusMessage });
                }
            }
        }
    }
}
